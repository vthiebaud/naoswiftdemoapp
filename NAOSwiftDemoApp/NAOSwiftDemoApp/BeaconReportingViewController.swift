//
//  BeaconReportingViewController.swift
//  NAOSwiftDemoApp
//
//  Created by Pole Star on 22/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import Foundation
import UIKit
import ModuleNAOSDK

class BeaconReportingViewController: UIViewController,
    NAOBeaconReportingHandleDelegate,
    NAOSyncDelegate,
    NAOSensorsDelegate
{
    
    var beaconReportingHandle: NAOBeaconReportingHandle?
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var synchronizeButton: UIButton!
    @IBOutlet weak var startStopButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        beaconReportingHandle = NAOBeaconReportingHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
    }
    
    
    
    @IBAction func onSynchronizeClicked(_ sender: Any) {
        Utils.showLoading("Synchronizing", controller: self)
        beaconReportingHandle?.synchronizeData(self)
    }
    
    
    @IBAction func onStartStopClicked(_ sender: Any) {
    if (startStopButton.titleLabel?.text == "Start") {
            statusLabel.text = "Beacon reporting service started."
            beaconReportingHandle?.start()
            startStopButton.setTitle("Stop", for: UIControl.State.normal)
        }
        else if (startStopButton.titleLabel?.text == "Stop") {
            startStopButton.setTitle("Start", for: UIControl.State.normal)
            statusLabel.text = "Beacon reporting service stopped."
            beaconReportingHandle?.stop()
        }
    }
    
    func didSynchronizationSuccess() {
        print(#function)
        statusLabel.text = "Synchronized. Beacon reporting ready."
        Utils.dismissLoading(self)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        self.statusLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func requiresWifiOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresWifiOn"
    }
    
    func requiresBLEOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresBLEOn"
    }
    
    func requiresLocationOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresLocationOn"
    }
    
    func requiresCompassCalibration() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresCompassCalibration"
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function + " : " + message)
        statusLabel.text = "Beacon reporting error : " + message
    }
}

