//
//  BeaconProximity.swift
//  NAOSwiftDemoApp
//
//  Created by Pole Star on 22/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import Foundation
import UIKit
import ModuleNAOSDK

class BeaconProximityViewController: UIViewController,
    NAOBeaconProximityHandleDelegate,
    NAOSyncDelegate,
    NAOSensorsDelegate
{
    var beaconProximityHandle: NAOBeaconProximityHandle?
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var synchronizeButton: UIButton!
    @IBOutlet weak var startStopButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        beaconProximityHandle = NAOBeaconProximityHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
    }
    
    @IBAction func onSynchronizedClicked(_ sender: Any) {
        Utils.showLoading("Synchronizing", controller: self)
        beaconProximityHandle?.synchronizeData(self)
    }
    
    @IBAction func onStartStopClicked(_ sender: Any) {
        if (startStopButton.titleLabel?.text == "Start") {
            statusLabel.text = "Beacon proximity service started."
            beaconProximityHandle?.start()
            startStopButton.setTitle("Stop", for: UIControl.State.normal)
        }
        else if (startStopButton.titleLabel?.text == "Stop") {
            startStopButton.setTitle("Start", for: UIControl.State.normal)
            statusLabel.text = "Beacon proximity service stopped."
            beaconProximityHandle?.stop()
        }
    }
    
    func didRangeBeacon(_ beaconPublicID: String!, withRssi rssi: Int32) {
        print(#function)
        statusLabel.text = "Ranged beacon " + beaconPublicID + " with rssi " + rssi.description
    }
    
    func didProximityChange(_ proximity: DBTBEACONSTATE, forBeacon beaconPublicID: String!) {
        print(#function)
        statusLabel.text = "Proximity state changed for beacon " + beaconPublicID
    }
    
    func didSynchronizationSuccess() {
        print(#function)
        statusLabel.text = "Synchronized. Beacon proximity ready."
        Utils.dismissLoading(self)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        self.statusLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func requiresWifiOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresWifiOn"
    }
    
    func requiresBLEOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresBLEOn"
    }
    
    func requiresLocationOn() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresLocationOn"
    }
    
    func requiresCompassCalibration() {
        print(#function)
        self.statusLabel.text = "NAO message : " + "requiresCompassCalibration"
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function + " : " + message)
        statusLabel.text = "Beacon proximity error : " + message
    }
}
