//
//  OthersViewController.swift
//  NAOSwiftDemoApp
//
//  Created by Pole Star on 22/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import Foundation
import UIKit
import ModuleNAOSDK

class OthersViewController: UIViewController,
    NAOLocationHandleDelegate,
    NAOSensorsDelegate,
    NAOSyncDelegate
{
    @IBOutlet weak var enableWakeButton: UIButton!
    @IBOutlet weak var uploadLogsButton: UIButton!
    @IBOutlet weak var startAllServicesButton: UIButton!
    
    static var wakePrefName: String! = "wake-lock"
    static var wakePrefValue: String! = "on"
    
    var locationHandle: NAOLocationHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        updateWakeButton()
    }
    
    private func updateWakeButton() {
        if (Utils.getPref(OthersViewController.wakePrefName) != nil) {
            enableWakeButton.setTitle("Disable wake lock", for: UIControl.State.normal)
        }
        else {
            enableWakeButton.setTitle("Enable wake lock", for: UIControl.State.normal)
        }
    }
    
    @IBAction func onEnableWakeClicked(_ sender: Any) {
        if (Utils.getPref(OthersViewController.wakePrefName) != nil) { // If already enabled, disabe it
            Utils.removePref(OthersViewController.wakePrefName)
            NAOServicesConfig.disableOnSiteWakeUp()
        }
        else {
            if (locationHandle == nil) {
                locationHandle = NAOLocationHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
                locationHandle!.start()
            }
            Utils.addPref(OthersViewController.wakePrefName, prefValue: "on") // If disabled, enable it
            NAOServicesConfig.enableOnSiteWakeUp()
        }
        updateWakeButton()
    }
    
    @IBAction func onUploadLogsClicked(_ sender: Any) {
        NAOServicesConfig.uploadNAOLogInfo()
        Utils.showToast(self, message: "Logs upload scheduled.")
    }
    
    @IBAction func onStartAllServicesClicked(_ sender: Any) {
        
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function)
    }
    
    func didLocationChange(_ location: CLLocation!) {
        //print(#function)
    }
    
    func didLocationStatusChanged(_ status: DBTNAOFIXSTATUS) {
        print(#function)
    }
    
    func requiresWifiOn() {
        print(#function)
    }
    
    func requiresBLEOn() {
        print(#function)
    }
    
    func requiresLocationOn() {
        print(#function)
    }
    
    func requiresCompassCalibration() {
        print(#function)
    }
    
    func didSynchronizationSuccess() {
        print(#function)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
    }
}

