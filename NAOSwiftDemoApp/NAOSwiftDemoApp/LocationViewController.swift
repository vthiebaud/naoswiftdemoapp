//
//  FirstViewController.swift
//  NAOSwiftDemoApp
//
//  Created by Alexia  on 21/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import UIKit
import ModuleNAOSDK
import UserNotifications

class LocationViewController: UIViewController,
    NAOSensorsDelegate,
    NAOSyncDelegate,
    NAOLocationHandleDelegate,
    UNUserNotificationCenterDelegate
{
    
    var locHandle: NAOLocationHandle?
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var synchroButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locHandle = NAOLocationHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
        Utils.requestNotificationAuth(self)
    }
    
    @IBAction func onSynchronizeClicked(_ sender: Any) {
        Utils.showLoading("Synchronizing", controller: self)
        locHandle?.synchronizeData(self)
    }
    
    @IBAction func onStartStopClicked(_ sender: Any) {
        if (startStopButton.titleLabel?.text == "Start") {
            Utils.showLoading("Starting", controller: self)
            startStopButton.setTitle("Stop", for: UIControl.State.normal)
            locHandle?.start()
        }
        else if (startStopButton.titleLabel?.text == "Stop") {
            startStopButton.setTitle("Start", for: UIControl.State.normal)
            locationLabel.text = "Stopped"
            locHandle?.stop()
        }
    }
    
    func requiresWifiOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresWifiOn"
    }
    
    func requiresBLEOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresBLEOn"
    }
    
    func requiresLocationOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresLocationOn"
    }
    
    func requiresCompassCalibration() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresCompassCalibration"
    }
    
    func didSynchronizationSuccess() {
        print(#function)
        locationLabel.text = "Synchronized. Location ready."
        Utils.dismissLoading(self)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        self.locationLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function)
        self.locationLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func didLocationChange(_ location: CLLocation!) {
        print("NAO location : " + location.description)
        self.locationLabel.text = location.coordinate.latitude.description + "\n" + location.coordinate.longitude.description
        Utils.dismissLoading(self)
    }
    
    func didLocationStatusChanged(_ status: DBTNAOFIXSTATUS) {
        print(#function)
        Utils.dismissLoading(self)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
        //handleWakeByNao(notification)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
        handleWakeByNao(notification)
    }
    
    private func handleWakeByNao(_ notif: UNNotification) {
        print(#function)
        if (notif.request.content.title == "NAO wake-lock") {
            print("REQUEST IS NAO WAKE LOCK");
        }
    }
}

