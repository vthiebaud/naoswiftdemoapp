//
//  Toolbox.swift
//  NAOSwiftDemoApp
//
//  Created by Pole Star on 22/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//


import Foundation
import UIKit
import UserNotifications

class Utils {
    
    static var naoKey: String! = "oarWVFpQUbccjy7ZBZCaOQ"

    static func showLoading(_ message: String, controller: UIViewController!) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        controller.present(alert, animated: true, completion: nil)
    }

    static func dismissLoading(_ controller: UIViewController!) {
        controller.dismiss(animated: false, completion: nil)
    }
    
    static func addPref(_ prefName: String!, prefValue: String!) {
        UserDefaults.standard.setValue(prefValue, forKey: prefName)
    }

    static func removePref(_ prefName: String!) {
        UserDefaults.standard.removeObject(forKey: prefName)
    }
    
    static func getPref(_ prefName: String!) -> String? {
        return UserDefaults.standard.string(forKey: prefName)
    }
    
    static func showToast(_ controller: UIViewController!, message : String) {
        let toastLabel = UILabel(frame: CGRect(x: controller.view.frame.size.width/2 - (controller.view.frame.size.width - 16)/2, y: controller.view.frame.size.height-100, width: controller.view.frame.size.width - 16, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        controller.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    static func requestNotificationAuth(_ delegate: UNUserNotificationCenterDelegate?) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                if (delegate != nil) {
                    UNUserNotificationCenter.current().delegate = delegate;
                    print("Delegate is : " + delegate!.description)
                }
                print("User notifications enabled")
            } else {
                print("User notifications disabled")
            }
        }
    }
    
    static func displayNotification(_ title: String!, message: String!) {
        let center = UNUserNotificationCenter.current()

        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.categoryIdentifier = "alarm"
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
}
