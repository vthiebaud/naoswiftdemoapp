//
//  SecondViewController.swift
//  NAOSwiftDemoApp
//
//  Created by Alexia  on 21/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import UIKit
import ModuleNAOSDK

class GeofencingViewController: UIViewController,
    NAOGeofenceHandleDelegate,
    NAOSyncDelegate,
    NAOSensorsDelegate
{
    
    var geofencingHandle: NAOGeofencingHandle?
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var synchronizeButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var geofenceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        geofencingHandle = NAOGeofencingHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
    }
    
    @IBAction func onSynchronizeClicked(_ sender: Any) {
        Utils.showLoading("Synchronizing", controller: self)
        geofencingHandle?.synchronizeData(self)
    }
    
    @IBAction func onStartStopClicked(_ sender: Any) {
        if (startStopButton.titleLabel?.text == "Start") {
            locationLabel.text = "Geofencing service started."
            startStopButton.setTitle("Stop", for: UIControl.State.normal)
            geofencingHandle?.start()
            
        }
        else if (startStopButton.titleLabel?.text == "Stop") {
            startStopButton.setTitle("Start", for: UIControl.State.normal)
            locationLabel.text = "Geofencing service stopped."
            geofencingHandle?.stop()
        }
    }
    
    func didSynchronizationSuccess() {
        print(#function)
        locationLabel.text = "Synchronized. Geofencing ready."
        Utils.dismissLoading(self)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        self.locationLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func requiresWifiOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresWifiOn"
    }
    
    func requiresBLEOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresBLEOn"
    }
    
    func requiresLocationOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresLocationOn"
    }
    
    func requiresCompassCalibration() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresCompassCalibration"
    }
    
    func didEnterGeofence(_ regionId: Int32, andName regionName: String!) {
        print(#function + " : " + regionName)
        geofenceLabel.text = "Entered geofence " + regionName
    }
    
    func didExitGeofence(_ regionId: Int32, andName regionName: String!) {
        print(#function + " : " + regionName)
        geofenceLabel.text = "Left geofence " + regionName
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function + " : " + message)
        locationLabel.text = "Error : " + message
    }
    
    func didFire(_ alert: NaoAlert!) {
        print(#function)
        locationLabel.text = "Alert received" + (alert.content != "" ? " : " + alert.content : "")
    }

}

