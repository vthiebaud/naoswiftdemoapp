//
//  AnalyticsViewController.swift
//  NAOSwiftDemoApp
//
//  Created by Pole Star on 22/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import Foundation
import UIKit
import ModuleNAOSDK

class AnalyticsViewController: UIViewController,
    NAOAnalyticsHandleDelegate,
    NAOSyncDelegate,
    NAOSensorsDelegate
{
    
    var analyticsHandle: NAOAnalyticsHandle?
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var synchronizeButton: UIButton!
    @IBOutlet weak var startStopButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        analyticsHandle = NAOAnalyticsHandle.init(key: Utils.naoKey, delegate: self, sensorsDelegate: self)
    }
    

    @IBAction func onSynchronizeClicked(_ sender: Any) {
    Utils.showLoading("Synchronizing", controller: self)
        analyticsHandle?.synchronizeData(self)
    }
    
    @IBAction func onStartStopClicked(_ sender: Any) {
    if (startStopButton.titleLabel?.text == "Start") {
            locationLabel.text = "Analytics service started."
            analyticsHandle?.start()
            startStopButton.setTitle("Stop", for: UIControl.State.normal)
        }
        else if (startStopButton.titleLabel?.text == "Stop") {
            startStopButton.setTitle("Start", for: UIControl.State.normal)
            locationLabel.text = "Analytics service stopped."
            analyticsHandle?.stop()
        }
    }
    
    func didSynchronizationSuccess() {
        print(#function)
        locationLabel.text = "Synchronized. Analytics ready."
        Utils.dismissLoading(self)
    }
    
    func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        self.locationLabel.text = "NAO error : " + message
        Utils.dismissLoading(self)
    }
    
    func requiresWifiOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresWifiOn"
    }
    
    func requiresBLEOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresBLEOn"
    }
    
    func requiresLocationOn() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresLocationOn"
    }
    
    func requiresCompassCalibration() {
        print(#function)
        self.locationLabel.text = "NAO message : " + "requiresCompassCalibration"
    }
    
    func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function + " : " + message)
        locationLabel.text = "Analytics error : " + message
    }
}

